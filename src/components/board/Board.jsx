import React from "react";
import BoardGame from "./BoardGame";
import Square from "../square/Square";

const rowStyle = {
  display: "flex",
};

const boardStyle = {
  backgroundColor: "#eee",
  width: "208px",
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  flexDirection: "column",
  border: "3px #eee solid",
};

const containerStyle = {
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
};

const instructionsStyle = {
  marginTop: "5px",
  marginBottom: "5px",
  fontWeight: "bold",
  fontSize: "16px",
};

const buttonStyle = {
  marginTop: "15px",
  marginBottom: "16px",
  width: "80px",
  height: "40px",
  backgroundColor: "#8acaca",
  color: "white",
  fontSize: "16px",
};

export default class Board extends React.Component {
  constructor(props) {
    super(props);
    this.board = new BoardGame(3);
    this.state = {
      currentPlayer: this.board.currentPlayer,
      winner: null,
      matrix: this.board.matrix,
    };
  }

  render() {
    return (
      <div style={containerStyle} className="gameBoard">
        <div id="statusArea" className="status" style={instructionsStyle}>
          Next player: <span>{this.board.currentPlayer}</span>
        </div>
        <div id="winnerArea" className="winner" style={instructionsStyle}>
          Winner:{" "}
          <span>
            {this.board.winnerPlayer ? this.board.winnerPlayer : "None"}
          </span>
        </div>
        <button style={buttonStyle} onClick={this.onReset.bind(this)}>
          Reset
        </button>
        <div style={boardStyle}>
          {this.state.matrix &&
            this.state.matrix.length &&
            this.state.matrix.map((row, rowIndex) => {
              return (
                <div className="board-row" style={rowStyle}>
                  {row &&
                    row.length &&
                    row.map((column, columnIndex) => {
                      return (
                        <Square
                          key={`${rowIndex}_${columnIndex}`}
                          row={rowIndex}
                          column={columnIndex}
                          value={column}
                          play={this.onSquareClick.bind(this)}
                          currentPlayer={this.state.currentPlayer}
                          disabled={this.state.winner}
                        />
                      );
                    })}
                </div>
              );
            })}
        </div>
      </div>
    );
  }

  onReset() {
    this.board.reset();
    this.updateMatrix();
  }

  onSquareClick(row, column) {
    if (this.state.winner) {
      return;
    }

    const winner = this.board.play(row, column);
    this.updateMatrix();

    if (winner) {
      this.setState({
        ...this.state,
        winner: winner,
      });
    }

    if (this.state.currentPlayer === this.board.currentPlayer) {
      return;
    }

    this.setState({
      ...this.state,
      currentPlayer: this.board.currentPlayer,
    });
  }

  updateMatrix() {
    this.setState({
      ...this.state,
      matrix: this.board.matrix,
    });
  }
}
