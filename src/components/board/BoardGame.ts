export default class BoardGame {
  private _players: string[];
  private _size: number;
  private _currentPlayer: string = null!;
  private _winnerPlayer: string = null!;
  private _principleDiagonalCols: number[][];
  private _secondaryDiagonalCols: number[][];
  private _currentPlayerIndex: number;
  private _matrix: string[][];

  constructor(n: number) {
    this._size = n;
    this._players = ["X", "O"];
    this._currentPlayerIndex = 0;
    this._currentPlayer = this._players[0];

    this._matrix = this.createBlankMatrix();
    this._principleDiagonalCols = this.calculatePrincipleDiagonalCols();
    this._secondaryDiagonalCols = this.calculateSecondaryDiagonalCols();
  }

  public get currentPlayer(): string {
    return this._currentPlayer;
  }

  public get winnerPlayer(): string {
    return this._winnerPlayer;
  }

  public get matrix(): string[][] {
    return this._matrix;
  }

  play(row: number, column: number): string | null {
    if (this._matrix[row][column] || this.winnerPlayer) {
      return null;
    }

    this._matrix[row][column] = this.currentPlayer;

    // Check if player won or not
    const isWon = this.checkWinner(row, column);

    if (isWon) {
      this._winnerPlayer = this.currentPlayer;
      return this._winnerPlayer;
    }

    this._currentPlayerIndex = this._currentPlayerIndex === 0 ? 1 : 0;
    this._currentPlayer = this._players[this._currentPlayerIndex];

    return null;
  }

  reset() {
    this._currentPlayerIndex = 0;
    this._currentPlayer = this._players[0];
    this._matrix = this.createBlankMatrix();
  }

  private checkWinner(row: number, column: number): boolean {
    return (
      this.checkHorizontalPositions(row) ||
      this.checkVerticalPositions(column) ||
      this.checkDiagonalPosition(this._principleDiagonalCols) ||
      this.checkDiagonalPosition(this._secondaryDiagonalCols)
    );
  }

  private checkHorizontalPositions(row: number): boolean {
    let count = 0;

    for (let i = 0; i < this._size; i++) {
      if (this._matrix[row][i] === this.currentPlayer) {
        count++;
      }
    }

    return count === this._size;
  }

  private checkVerticalPositions(column: number): boolean {
    let count = 0;

    for (let i = 0; i < this._size; i++) {
      if (this._matrix[i][column] === this.currentPlayer) {
        count++;
      }
    }

    return count === this._size;
  }

  private checkDiagonalPosition(diagonalTypeValue: number[][]): boolean {
    let count = 0;

    for (let i = 0; i < this._size; i++) {
      const digonalPos = diagonalTypeValue[i];

      if (this._matrix[digonalPos[0]][digonalPos[1]] === this.currentPlayer) {
        count++;
      }
    }

    return count === this._size;
  }

  private createBlankMatrix(): string[][] {
    let matrix = [] as any;
    for (let i = 0; i < this._size; i++) {
      matrix.push([]);
      for (let j = 0; j < this._size; j++) {
        matrix[i][j] = "";
      }
    }
    return matrix;
  }

  private calculatePrincipleDiagonalCols(): number[][] {
    const cols = [] as number[][];

    for (let i = 0; i < this._size; i++) {
      cols.push([i, i]);
    }

    return cols;
  }

  private calculateSecondaryDiagonalCols(): number[][] {
    const cols = [];
    let k = this._size - 1;

    for (let i = 0; i < this._size; i++) {
      cols.push([i, k--]);
    }

    return cols;
  }
}
