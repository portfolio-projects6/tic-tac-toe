import React from "react";

let squareStyle = {
  width: "60px",
  height: "60px",
  backgroundColor: "#ddd",
  margin: "4px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontSize: "20px",
  color: "white",
};

export default class Square extends React.Component {
  onBoxClick(e) {
    e.preventDefault();

    if (this.props.disabled) {
      return;
    }

    if (this.props.value) {
      return;
    }

    this.props.play(this.props.row, this.props.column);
  }

  render() {
    console.log(this.props.value);
    return (
      <div
        className="square"
        style={squareStyle}
        onClick={this.onBoxClick.bind(this)}
      >
        {this.props.value}
      </div>
    );
  }
}
